﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class v4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "quantity",
                table: "ingredients",
                newName: "Quantity");

            migrationBuilder.RenameColumn(
                name: "name",
                table: "ingredients",
                newName: "Name");

            migrationBuilder.AddColumn<string>(
                name: "PhotoName",
                table: "recipes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhotoName",
                table: "recipes");

            migrationBuilder.RenameColumn(
                name: "Quantity",
                table: "ingredients",
                newName: "quantity");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "ingredients",
                newName: "name");
        }
    }
}
