﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class PersistenceRecipeDBContextv3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ingredient_recipes_RecipeID",
                table: "Ingredient");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Ingredient",
                table: "Ingredient");

            migrationBuilder.RenameTable(
                name: "Ingredient",
                newName: "ingredients");

            migrationBuilder.RenameIndex(
                name: "IX_Ingredient_RecipeID",
                table: "ingredients",
                newName: "IX_ingredients_RecipeID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ingredients",
                table: "ingredients",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_ingredients_recipes_RecipeID",
                table: "ingredients",
                column: "RecipeID",
                principalTable: "recipes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ingredients_recipes_RecipeID",
                table: "ingredients");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ingredients",
                table: "ingredients");

            migrationBuilder.RenameTable(
                name: "ingredients",
                newName: "Ingredient");

            migrationBuilder.RenameIndex(
                name: "IX_ingredients_RecipeID",
                table: "Ingredient",
                newName: "IX_Ingredient_RecipeID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Ingredient",
                table: "Ingredient",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Ingredient_recipes_RecipeID",
                table: "Ingredient",
                column: "RecipeID",
                principalTable: "recipes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
