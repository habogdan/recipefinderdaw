﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class v9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "recipes",
                columns: new[] { "ID", "AccountID", "Description", "Name", "PhotoName" },
                values: new object[] { -1, 1, "Intinzi foaie, adaugi mere, intinzi foaie, bagi la cuptor", "Placinta cu mere", "cooking.jpeg" });

            migrationBuilder.InsertData(
                table: "recipes",
                columns: new[] { "ID", "AccountID", "Description", "Name", "PhotoName" },
                values: new object[] { -2, 1, "Fierbi apa, pui paste la fiert 11 min, scoti si pui sos", "Farfale", "cooking.jpeg" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "recipes",
                keyColumn: "ID",
                keyValue: -2);

            migrationBuilder.DeleteData(
                table: "recipes",
                keyColumn: "ID",
                keyValue: -1);
        }
    }
}
