﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using API.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RecipesController : ControllerBase
    {
        private readonly IRecipeRepository repository;
        private readonly ILogger<WeatherForecastController> _logger;

        public RecipesController(IRecipeRepository repository, ILogger<WeatherForecastController> logger)
        {
            this.repository = repository;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<Recipe>> GetRecipe()
        {
            return repository.GetRecipes().ToList();
        }

        [HttpGet("{recipeID}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Recipe> GetRecipe(int recipeID)
        {
            Recipe r = repository.GetRecipe(recipeID);
            if (r != null)
                return r;
            return NotFound();
        }

        [HttpDelete("{recipeID}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Recipe> RemoveRecipe(int recipeID)
        {
            return repository.RemoveRecipe(recipeID);
        }

        [HttpPut("{recipeID}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Recipe> UpdateRecipe(int recipeID, [FromBody]Recipe recipe)
        {
            recipe.ID = recipeID;
            return repository.UpdateRecipe(recipe);
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Recipe> CreateRecipe([FromBody]Recipe recipe)
        {
            return repository.CreateRecipe(recipe);
        }

        [HttpGet("Favorites/{accountID}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<Favorite>> GetFavoriteRecipes(string accountID)
        {
            List<Favorite> FavoriteRecipes = repository.GetFavoriteRecipes(accountID).ToList();


            return FavoriteRecipes;
        }

        [HttpPost("Favorites/{recipeID}/{userID}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Favorite> AddFavorite(int recipeID, string userID)
        {
            return repository.AddtoFavorite(userID,recipeID);
        }

        [HttpDelete("Favorites/{recipeID}/{userID}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Favorite> RemoveFavorite(int recipeID, string userID)
        {
            return repository.RemoveFavoriteRecipe(userID, recipeID);
        }
    }
}