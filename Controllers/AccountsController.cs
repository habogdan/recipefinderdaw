﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using API.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountsController : ControllerBase
    {
        private readonly IRecipeRepository repository;
        private readonly ILogger<WeatherForecastController> _logger;

        public AccountsController(IRecipeRepository repository, ILogger<WeatherForecastController> logger)
        {
            this.repository = repository;
            _logger = logger;
        }

        [HttpGet("{accountID}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Account>  GetAccount ( int accountID)
        {
            Account account = repository.GetAccount(accountID);

            return account;
        }
    }
}
