﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using API.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IngredientsController : ControllerBase
    {
        private readonly IRecipeRepository repository;
        private readonly ILogger<WeatherForecastController> _logger;

        public IngredientsController(IRecipeRepository repository, ILogger<WeatherForecastController> logger)
        {
            this.repository = repository;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<Ingredient>> GetRecipe()
        {
            return repository.GetIngredients().ToList();
        }

        [HttpGet("{ingredientID}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Ingredient> GetIngredient(int ingredientID)
        {
            Ingredient i = repository.GetIngredient(ingredientID);
            if (i != null)
                return i;
            return NotFound();
        }

        [HttpDelete("{ingredientID}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Ingredient> RemoveIngredient(int ingredientID)
        {
            return repository.RemoveIngredient(ingredientID);
        }

        [HttpPut("{ingredientID}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Ingredient> UpdateIngredient(int ingredientID, [FromBody]Ingredient ingredient)
        {
            ingredient.ID = ingredientID;
            return repository.UpdateIngredient(ingredient);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Ingredient> AddIngredient([FromBody]Ingredient ingredient)
        {
            return repository.AddIngredient(ingredient);
        }
    }
}