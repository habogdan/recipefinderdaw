﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Recipe
    {
        [Required]
        public int ID { get; set; }
        [Required]
        public int AccountID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public string PhotoName { get; set; }

        public Account Account { get; set; }

        public ICollection<Ingredient> Ingredients { get; set; }

        public Recipe()
        {
        }
        public Recipe( int id, string name, string description)
        {
            ID = id;
            Name = name;
            Description = description;
        }
    }
}
