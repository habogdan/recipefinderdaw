﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Favorite
    {
        [Required]
        public int ID { get; set; }
        public string UserID { get; set; }
        public int RecipeID { get; set; }
    }
}
