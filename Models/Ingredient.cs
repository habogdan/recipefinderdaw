﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Ingredient
    {
        [Required]
        public int ID { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }

        public int RecipeID { get; set; }

    }
}
