﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Account
    {
        [Required]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }

        public ICollection<Recipe> Recipes { get; set; }
    }
}
