﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Persistence
{
    public interface IRecipeRepository
    {
         Recipe GetRecipe(int id);
         IEnumerable<Recipe> GetRecipes();
         Recipe CreateRecipe(Recipe recipe);
         Recipe UpdateRecipe(Recipe recipe);
         Recipe RemoveRecipe(int id);

        Ingredient GetIngredient(int id);
        IEnumerable<Ingredient> GetIngredients();
        Ingredient AddIngredient(Ingredient ingredient);
        Ingredient UpdateIngredient(Ingredient ingredient);
        Ingredient RemoveIngredient(int id);

        Account GetAccount(int id);
        Account CreateAccount(Account account);
        Account UpdateAccount(Account account);
        Account RemoveAccount(int id);

        IEnumerable<Recipe> GetAccountRecipe(int accountID);
        IEnumerable<Recipe> GetIngredientRecipe(int ingredientID);


        IEnumerable<Favorite> GetFavoriteRecipes(string userID);
        Favorite AddtoFavorite(string accountID, int recipeID);
        Favorite RemoveFavoriteRecipe(string userID, int recipeID);
    }
    
}
