﻿using API.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Persistence
{
    public class RecipeDBContext : DbContext
    {

        public RecipeDBContext ( DbContextOptions<RecipeDBContext> options):base(options)
        {

        }


        public DbSet<Account> accounts{ get; set; }
        public DbSet<Recipe> recipes{ get; set; }
        public DbSet<Ingredient> ingredients{ get; set; }
        public DbSet<Favorite> favorites { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Recipe>().HasData(
                new Recipe
                {
                    ID = -1,
                    Name = "Placinta cu mere",
                    Description = "Intinzi foaie, adaugi mere, intinzi foaie, bagi la cuptor",
                    AccountID = 1,
                    PhotoName = "cooking.jpeg"
                },
                new Recipe
                {
                    ID = -2,
                    Name = "Farfale",
                    Description = "Fierbi apa, pui paste la fiert 11 min, scoti si pui sos",
                    AccountID = 1,
                    PhotoName = "cooking.jpeg"
                }

                );
        }

    }
}
