﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using Microsoft.EntityFrameworkCore;

namespace API.Persistence
{
    public class RecipeRepository : IRecipeRepository
    {
        private RecipeDBContext context;

        public RecipeRepository(RecipeDBContext context)
        {
            this.context = context;
        }

        public Ingredient AddIngredient(Ingredient ingredient)
        {
            var entity = context.Add(ingredient);
            context.SaveChanges();

            return entity.Entity;
        }

        public Account CreateAccount(Account account)
        {
             var entity = context.Add(account);
            context.SaveChanges();


            return entity.Entity;
        }

        public Recipe CreateRecipe(Recipe recipe)
        {
            var entity = context.Add(recipe);
            context.SaveChanges();

            return entity.Entity;
        }

        public Account GetAccount(int id)
        {
            return context.accounts.FirstOrDefault(a=>a.ID == id);
        }

        public IEnumerable<Recipe> GetAccountRecipe(int accountID)
        {
            return GetAccount(accountID).Recipes;
        }

        public Ingredient GetIngredient(int id)
        {
            return context.ingredients.FirstOrDefault(a => a.ID == id);
        }

        public IEnumerable<Recipe> GetIngredientRecipe(int ingredientID)
        {
            Ingredient i = GetIngredient(ingredientID); 
            return context.recipes.Where(r => r.Ingredients.Contains(i)).ToList();
        }

        public IEnumerable<Ingredient> GetIngredients()
        {
            return context.ingredients;
        }

        public Recipe GetRecipe(int id)
        {
            return context.recipes.FirstOrDefault(a => a.ID == id);
        }

        public IEnumerable<Recipe> GetRecipes()
        {
            return context.recipes;
        }

        public Account RemoveAccount(int id)
        {
            var account = GetAccount(id);
            context.Remove(account);
            context.SaveChanges();
            return account;
        }

        public Ingredient RemoveIngredient(int id)
        {
            var ingredient = GetIngredient(id);
            context.Remove(ingredient);
            context.SaveChanges();
            return ingredient;
        }

        public Recipe RemoveRecipe(int id)
        {
            var recipe = GetRecipe(id);
            context.Remove(recipe);
            context.SaveChanges();
            return recipe;
        }

        public Account UpdateAccount(Account account)
        {
            context.Entry(account).State = EntityState.Modified;
            context.SaveChanges();
            return account;

        }

        public Ingredient UpdateIngredient(Ingredient ingredient)
        {
            context.Entry(ingredient).State = EntityState.Modified;
            context.SaveChanges();
            return ingredient;
        }

        public Recipe UpdateRecipe(Recipe recipe)
        {
            context.Entry(recipe).State = EntityState.Modified;
            context.SaveChanges();
            return recipe;
        }

        public IEnumerable<Favorite> GetFavoriteRecipes(string userID)
        {
            return context.favorites.Where( r => r.UserID == userID).ToList();
        }

        public Favorite AddtoFavorite(string userID, int recipeID)
        {
            var existent = context.favorites.FirstOrDefault(r => r.UserID == userID && r.RecipeID == recipeID);
            if (existent != null)
            {
                return existent;
            }
            var favorite = new Favorite();
            favorite.RecipeID = recipeID;
            favorite.UserID = userID;
            var entity = context.Add(favorite);
            context.SaveChanges();

            return entity.Entity;
        }

        public Favorite RemoveFavoriteRecipe(string userID, int recipeID)
        {
            var favorites = context.favorites.FirstOrDefault(r => r.UserID == userID && r.RecipeID == recipeID);
            context.Remove(favorites);
            context.SaveChanges();
            return favorites;
        }
    }
}
